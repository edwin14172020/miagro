package com.sadycr.miagro.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.sadycr.miagro.FiltroBuscador.CustomFilterC;
import com.sadycr.miagro.Models.Carga;
import com.sadycr.miagro.R;

import java.util.ArrayList;

public class ListViewCargaAdapter  extends BaseAdapter implements Filterable {
    Context context;
    public ArrayList<Carga> cargaData, filterList;
    LayoutInflater layoutInflater;
    Carga cargamodel;
    CustomFilterC filterC;

    public ListViewCargaAdapter(Context context, ArrayList<Carga> cargaData){
        this.context = context;
        this.cargaData = cargaData;
        this.filterList = cargaData;
        layoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );
    }


    @Override
    public int getCount() {
        return cargaData.size();
    }

    @Override
    public Object getItem(int position) {
        return cargaData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View romView = convertView;
        if (romView==null){
            romView = layoutInflater.inflate(R.layout.lista_carga,
            null,
            true);
        }

        TextView finca = romView.findViewById(R.id.finca);
        TextView cultivo = romView.findViewById(R.id.cultivo);
        TextView recolector = romView.findViewById(R.id.recolector);
        TextView pesocarga = romView.findViewById(R.id.pesocarga);
        TextView encargado = romView.findViewById(R.id.encargada);
        TextView fecha = romView.findViewById(R.id.fecharegistro);

        cargamodel = cargaData.get(position);
        finca.setText(cargamodel.getFinca());
        cultivo.setText(cargamodel.getCultivo());
        recolector.setText(cargamodel.getRecolector());
        pesocarga.setText(cargamodel.getCarga());
        encargado.setText(cargamodel.getEncargado());
        fecha.setText(cargamodel.getFecharegistro());

        return romView;
    }

    @Override
    public Filter getFilter() {
        if (filterC==null){
            filterC = new CustomFilterC(filterList, this);
        }
        return filterC;
    }
}
