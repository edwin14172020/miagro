package com.sadycr.miagro.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.sadycr.miagro.FiltroBuscador.CustomFilterCUL;
import com.sadycr.miagro.Models.Cultivos;
import com.sadycr.miagro.R;

import java.util.ArrayList;

public class ListViewCultivosAdapter extends BaseAdapter implements Filterable {

    Context context;
    public ArrayList<Cultivos> CultivosData, filterList;
    LayoutInflater layoutInflater;
    Cultivos CultivosModel;
    CustomFilterCUL filterCUL;

    public ListViewCultivosAdapter(Context context, ArrayList<Cultivos> CultivosData) {
        this.context = context;
        this.CultivosData = CultivosData;
        this.filterList = CultivosData;
        layoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );
    }


    @Override
    public int getCount() {
        return CultivosData.size();
    }

    @Override
    public Object getItem(int position) {
        return CultivosData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if(rowView==null){
            rowView = layoutInflater.inflate(R.layout.lista_cultivos,
                    null,
                    true);
        }
        TextView nombresFinca = rowView.findViewById(R.id.nombresFinca);
        TextView nombrecultivo = rowView.findViewById(R.id.nombrecultivo);
        TextView hectareas = rowView.findViewById(R.id.hectareas);
        TextView UbicacionFinca = rowView.findViewById(R.id.UbicacionFinca);
        TextView ArbolesH = rowView.findViewById(R.id.ArbolesH);
        TextView TipoSuelo = rowView.findViewById(R.id.TipoSuelo);
        TextView TipoAbono = rowView.findViewById(R.id.TipoAbono);
        TextView TipoCultivo = rowView.findViewById(R.id.TipoCultivo);
        TextView Textura = rowView.findViewById(R.id.Textura);
        TextView PHsuelo = rowView.findViewById(R.id.PHsuelo);
        TextView TipoFumigacion = rowView.findViewById(R.id.TipoFumigacion);
        TextView ProductosFumigacion = rowView.findViewById(R.id.ProductosFumigacion);
        TextView fecharegistro = rowView.findViewById(R.id.fecharegistro);

        CultivosModel = CultivosData.get(position);
        nombresFinca.setText(CultivosModel.getNombreFinca());
        nombrecultivo.setText(CultivosModel.getNombreCultivo());
        hectareas.setText(CultivosModel.getHectareas());
        UbicacionFinca.setText(CultivosModel.getUbicacionFinca());
        ArbolesH.setText(CultivosModel.getArbolesH());
        PHsuelo.setText(CultivosModel.getPHsuelo());
        TipoSuelo.setText(CultivosModel.getTipoSuelo());
        TipoAbono.setText(CultivosModel.getTipoAbono());
        TipoCultivo.setText(CultivosModel.getTipoCultivo());
        Textura.setText(CultivosModel.getTextura());
        TipoFumigacion.setText(CultivosModel.getTipoFumigacion());
        ProductosFumigacion.setText(CultivosModel.getProductosFumigacion());
        fecharegistro.setText(CultivosModel.getFecharegistro());

        return rowView;
    }

    @Override
    public Filter getFilter() {
        if (filterCUL == null){
            filterCUL = new CustomFilterCUL(filterList, this);
    }
        return filterCUL;
    }
}
