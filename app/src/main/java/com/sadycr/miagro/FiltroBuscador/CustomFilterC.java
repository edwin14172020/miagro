package com.sadycr.miagro.FiltroBuscador;

import android.widget.Filter;

import com.sadycr.miagro.Adaptadores.ListViewCargaAdapter;
import com.sadycr.miagro.Models.Carga;
import com.sadycr.miagro.Models.Persona;

import java.util.ArrayList;

public class CustomFilterC extends Filter {
    ArrayList<Carga> filterlist;
    ListViewCargaAdapter adapter;

    public CustomFilterC (ArrayList<Carga> filterlist, ListViewCargaAdapter adapter) {
        this.filterlist = filterlist;
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constrain) {
        FilterResults results = new FilterResults();
        if (constrain!=null &&constrain.length()>0){
            constrain = constrain.toString().toUpperCase();
            ArrayList<Carga> modeloFiltrado = new ArrayList<>();
            for(int i=0;i<filterlist.size();i++){
                if (filterlist.get(i).getFinca().toUpperCase().contains(constrain)|| filterlist.get(i).getCultivo().toUpperCase().contains(constrain)|| filterlist.get(i).getRecolector().toUpperCase().contains(constrain)||filterlist.get(i).getCarga().toUpperCase().contains(constrain) ||filterlist.get(i).getFecharegistro().toUpperCase().contains(constrain)){
                    modeloFiltrado.add(filterlist.get(i));
                }
            }
            results.count = modeloFiltrado.size();
            results.values = filterlist;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.cargaData = (ArrayList<Carga>) results.values;
        adapter.notifyDataSetChanged();
    }
}
