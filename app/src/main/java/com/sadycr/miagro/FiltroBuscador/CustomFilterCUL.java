package com.sadycr.miagro.FiltroBuscador;

import android.widget.Filter;

import com.sadycr.miagro.Adaptadores.ListViewCultivosAdapter;
import com.sadycr.miagro.Models.Cultivos;

import java.util.ArrayList;

public class CustomFilterCUL extends Filter {
    ArrayList<Cultivos> filterList;
    ListViewCultivosAdapter adapter;

    public CustomFilterCUL(ArrayList<Cultivos> filterList, ListViewCultivosAdapter adapter) {
        this.filterList = filterList;
        this.adapter = adapter;
    }


    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if(constraint!=null &&constraint.length()>0){
            constraint = constraint.toString().toUpperCase();
            ArrayList<Cultivos> modeloFiltrado = new ArrayList<>();
            for(int i=0;i<filterList.size();i++){
                if(filterList.get(i).getNombreFinca().toUpperCase().contains(constraint) || filterList.get(i).getNombreCultivo().toUpperCase().contains(constraint) ||
                        filterList.get(i).getHectareas().toUpperCase().contains(constraint) || filterList.get(i).getUbicacionFinca().toUpperCase().contains(constraint) ||
                        filterList.get(i).getTipoSuelo().toUpperCase().contains(constraint) || filterList.get(i).getTipoAbono().toUpperCase().contains(constraint) ||
                        filterList.get(i).getTipoCultivo().toUpperCase().contains(constraint) || filterList.get(i).getTextura().toUpperCase().contains(constraint) ||
                        filterList.get(i).getTipoFumigacion().toUpperCase().contains(constraint) || filterList.get(i).getProductosFumigacion().toUpperCase().contains(constraint) ||
                        filterList.get(i).getPHsuelo().toUpperCase().contains(constraint) ||filterList.get(i).getArbolesH().toUpperCase().contains(constraint) ||filterList.get(i).getFecharegistro().toUpperCase().contains(constraint)) {
                    modeloFiltrado.add(filterList.get(i));
                }
            }
            results.count = modeloFiltrado.size();
            results.values = modeloFiltrado;
        }else{
            results.count = filterList.size();
            results.values = filterList;
        }
        return  results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.CultivosData = (ArrayList<Cultivos>) results.values;
        adapter.notifyDataSetChanged();
    }
}
