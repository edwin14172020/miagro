package com.sadycr.miagro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class InformesActivity extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_informes);

        toolbar = findViewById(R.id.toolsbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_perso, menu);
        MenuItem menuItem = menu.findItem(R.id.buscar);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_agregar:
                break;
            case R.id.menu_guardar:
                break;
            case R.id.menu_eliminar:
                break;
            case R.id.home:
                goatras();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goatras() {
        Intent intentatras = new Intent(this, MainActivity.class);
        startActivity(intentatras);
    }
}
