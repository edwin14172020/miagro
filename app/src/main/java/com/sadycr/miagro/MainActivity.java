package com.sadycr.miagro;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    TextView textname;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;


    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolsbar);
        textname = findViewById(R.id.et_name);
        setSupportActionBar(toolbar);

        toggle = setUpDrawerToggle();
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

    }

    private ActionBarDrawerToggle setUpDrawerToggle() {
        return new ActionBarDrawerToggle(this,
                drawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close);
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        SelectItemNav(item);
        return false;
    }

    private void SelectItemNav(MenuItem item) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        int id = item.getItemId();
        if (id == R.id.formulariocultivo) {
            gocultivo();
        } else if(id == R.id.formulariocarga) {
            gocarga();
        } else if(id == R.id.informes) {
            goinformes();
        } else if(id == R.id.trabajador){
            gotrabajor();
        } else if(id == R.id.cerrarsesion) {
            gocerrar();
        }
        setTitle(item.getTitle());
        drawerLayout.closeDrawers();
    }

    private void gocerrar(){
        FirebaseAuth.getInstance().signOut();
        Toast.makeText(MainActivity.this, "Sesion cerrada", Toast.LENGTH_SHORT).show();
        gologing();
    }

    private void gotrabajor() {
        Intent intenttrabajor = new Intent(this, TrabajadorActivity.class);
        startActivity(intenttrabajor);
    }

    private void gocarga() {
        Intent intentcarga = new Intent(this, anadirCargaActivity.class);
        startActivity(intentcarga);
    }

    private void gocultivo() {
        Intent intentcultivo = new Intent(this, activity_anadir_cultivo.class);
        startActivity(intentcultivo);
    }

    private void goinformes() {
        Intent intentinformes = new Intent(this, InformesActivity.class);
        startActivity(intentinformes);
    }

    private void gologing() {
        Intent i = new Intent(this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
