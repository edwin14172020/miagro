package com.sadycr.miagro.Models;

public class Cultivos {

    private String Idcultivo;
    private String NombreFinca;
    private String NombreCultivo;
    private String Hectareas;
    private String UbicacionFinca;
    private String TipoSuelo;
    private String PHsuelo;
    private String ArbolesH;
    private String TipoAbono;
    private String TipoCultivo;
    private String Textura;
    private String TipoFumigacion;
    private String ProductosFumigacion;
    private String fecharegistro;
    private long timestamp;

    public String getIdcultivo() {
        return Idcultivo;
    }

    public void setIdcultivo(String Idcultivo) {
        this.Idcultivo = Idcultivo;
    }

    public String getPHsuelo() {
        return PHsuelo;
    }

    public void setPHsuelo(String PHsuelo) {
        this.PHsuelo = PHsuelo;
    }

    public String getTipoCultivo() {
        return TipoCultivo;
    }

    public void setTipoCultivo(String tipoCultivo) {
        TipoCultivo = tipoCultivo;
    }

    public String getArbolesH() {
        return ArbolesH;
    }

    public void setArbolesH(String arbolesH) {
        ArbolesH = arbolesH;
    }

    public String getNombreFinca() {
        return NombreFinca;
    }

    public void setNombreFinca(String nombreFinca) {
        NombreFinca = nombreFinca;
    }

    public String getNombreCultivo() {
        return NombreCultivo;
    }

    public void setNombreCultivo(String nombreCultivo) {
        NombreCultivo = nombreCultivo;
    }

    public String getHectareas() {
        return Hectareas;
    }

    public void setHectareas(String hectareas) {
        Hectareas = hectareas;
    }

    public String getUbicacionFinca() {
        return UbicacionFinca;
    }

    public void setUbicacionFinca(String ubicacionFinca) {
        UbicacionFinca = ubicacionFinca;
    }

    public String getTipoSuelo() {
        return TipoSuelo;
    }

    public void setTipoSuelo(String tipoSuelo) {
        TipoSuelo = tipoSuelo;
    }

    public String getTipoAbono() {
        return TipoAbono;
    }

    public void setTipoAbono(String tipoAbono) {
        TipoAbono = tipoAbono;
    }

    public String getTextura() {
        return Textura;
    }

    public void setTextura(String textura) {
        Textura = textura;
    }

    public String getTipoFumigacion() {
        return TipoFumigacion;
    }

    public void setTipoFumigacion(String tipoFumigacion) {
        TipoFumigacion = tipoFumigacion;
    }

    public String getProductosFumigacion() {
        return ProductosFumigacion;
    }

    public void setProductosFumigacion(String productosFumigacion) {
        ProductosFumigacion = productosFumigacion;
    }

    public String getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(String fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return NombreFinca;
    }
}

