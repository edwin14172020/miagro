package com.sadycr.miagro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class RecuperarPassActivity extends AppCompatActivity {

    private EditText mEditTextEmail;
    private Button mButtonResetPassword;
    AppCompatButton btninicio;

    private String email = "";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_pass);

        principal();

        mAuth = FirebaseAuth.getInstance();

        mEditTextEmail = (EditText) findViewById(R.id.editTextEmail);
        mButtonResetPassword = (Button) findViewById(R.id.btnResetPassword);

        mButtonResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = mEditTextEmail.getText().toString();

                if (!email.isEmpty()){
                    resetPassword();
                }
                else {
                    Toast.makeText(RecuperarPassActivity.this, "Debe ingresar el email", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void principal() {

        btninicio = findViewById(R.id.btn_inicio);
        btninicio.setOnClickListener((evt) -> {
            onInicioClick();
        });

    }
    private void onInicioClick(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void resetPassword() {

        mAuth.setLanguageCode("es");
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()){
                    Toast.makeText(RecuperarPassActivity.this, "Se ha enviado un correo para reestablecer tu contraseña", Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(RecuperarPassActivity.this, "No se pudo enviar el correo de reestablecer contraseña", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}