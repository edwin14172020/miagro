package com.sadycr.miagro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sadycr.miagro.Adaptadores.ListViewCultivosAdapter;
import com.sadycr.miagro.Models.Cultivos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import com.sadycr.miagro.Models.Cultivos;

public class activity_anadir_cultivo extends AppCompatActivity {
    private ArrayList<Cultivos> listCultivos= new ArrayList<Cultivos>();
    ArrayAdapter<Cultivos> arrayAdapterCultivos;
    ListViewCultivosAdapter listViewCultivosAdapter;
    LinearLayout linearLayoutEditarCultivos;
    ListView listViewCultivos;

    Toolbar toolbar;

    EditText inputNombreFinca, inputNombreCultivo, inputHectareas, inputUbicacionFinca,
            inputTipoCultivo, inputTipoAbono, inputArbolesH, inputTipoSuelo,
            inputPHsuelo,inputTextura, inputTipoFumigacion, inputProductosFumigacion;
    Button btnCancelar;

    Cultivos CultivosSeleccionados;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir_cultivo);

        inputNombreFinca = findViewById(R.id.inputNombreFinca);
        inputNombreCultivo = findViewById(R.id.inputNombreCultivo);
        inputHectareas = findViewById(R.id.inputHectareas);
        inputUbicacionFinca = findViewById(R.id.inputUbicacionFinca);
        inputArbolesH = findViewById(R.id.inputArbolesH);
        inputTipoCultivo = findViewById(R.id.inputTipoCultivo);
        inputTipoSuelo = findViewById(R.id.inputTipoSuelo);
        inputTipoAbono = findViewById(R.id.inputTipoAbono);
        inputPHsuelo = findViewById(R.id.inputPHsuelo);
        inputTextura = findViewById(R.id.inputTextura);
        inputTipoFumigacion = findViewById(R.id.inputTipoFumigacion);
        inputProductosFumigacion = findViewById(R.id.inputProductosFumigacion);
        btnCancelar = findViewById(R.id.btnCancelar);

        toolbar = findViewById(R.id.toolsbar);
        setSupportActionBar(toolbar);

        listViewCultivos = findViewById(R.id.listViewCultivos);
        linearLayoutEditarCultivos = findViewById(R.id.linearLayoutEditarCultivos);

        listViewCultivos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CultivosSeleccionados = (Cultivos) parent.getItemAtPosition(position);
                inputNombreFinca.setText(CultivosSeleccionados.getNombreFinca());
                inputNombreCultivo.setText(CultivosSeleccionados.getNombreCultivo());
                inputHectareas.setText(CultivosSeleccionados.getHectareas());
                inputUbicacionFinca.setText(CultivosSeleccionados.getUbicacionFinca());
                inputArbolesH.setText(CultivosSeleccionados.getArbolesH());
                inputTipoCultivo.setText(CultivosSeleccionados.getTipoCultivo());
                inputTipoSuelo.setText(CultivosSeleccionados.getTipoSuelo());
                inputTipoAbono.setText(CultivosSeleccionados.getTipoAbono());
                inputPHsuelo.setText(CultivosSeleccionados.getPHsuelo());
                inputTextura.setText(CultivosSeleccionados.getTextura());
                inputTipoFumigacion.setText(CultivosSeleccionados.getTipoFumigacion());
                inputProductosFumigacion.setText(CultivosSeleccionados.getProductosFumigacion());

                linearLayoutEditarCultivos.setVisibility(View.VISIBLE);
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutEditarCultivos.setVisibility(View.GONE);
                CultivosSeleccionados = null;
            }
        });

        inicializarFirebase();
        listarCultivos();
    }

    private void  inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    private void listarCultivos(){
        databaseReference.child("Cultivos").orderByChild("timestamp").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listCultivos.clear();
                for(DataSnapshot objSnaptshot : dataSnapshot.getChildren()){
                    Cultivos c = objSnaptshot.getValue(Cultivos.class);
                    listCultivos.add(c);
                }
                listViewCultivosAdapter = new ListViewCultivosAdapter(activity_anadir_cultivo.this, listCultivos);
                listViewCultivos.setAdapter(listViewCultivosAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_perso, menu);

        MenuItem menuItem = menu.findItem(R.id.buscar);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                listViewCultivosAdapter.getFilter().filter(newText);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        String nombrefinca = inputNombreFinca.getText().toString();
        String nombrecultivo = inputNombreCultivo.getText().toString();
        String hectareas = inputHectareas.getText().toString();
        String ubicacionfinca = inputUbicacionFinca.getText().toString();
        String arbolesh = inputArbolesH.getText().toString();
        String tipocultivo = inputTipoCultivo.getText().toString();
        String tiposuelo = inputTipoSuelo.getText().toString();
        String tipoabono = inputTipoAbono.getText().toString();
        String phsuelo = inputPHsuelo.getText().toString();
        String textura = inputTextura.getText().toString();
        String tipofumigacion = inputTipoFumigacion.getText().toString();
        String productosfumigacion = inputProductosFumigacion.getText().toString();
        switch (item.getItemId()){
            case R.id.menu_agregar:
                insertarCultivo();
                break;
            case R.id.menu_guardar:
                if(CultivosSeleccionados != null){
                    if(validarInputs()==false){
                        Cultivos c = new Cultivos();
                        c.setIdcultivo(CultivosSeleccionados.getIdcultivo());
                        c.setNombreFinca(nombrefinca);
                        c.setNombreCultivo(nombrecultivo);
                        c.setHectareas(hectareas);
                        c.setUbicacionFinca(ubicacionfinca);
                        c.setArbolesH(arbolesh);
                        c.setTipoCultivo(tipocultivo);
                        c.setTipoSuelo(tiposuelo);
                        c.setTipoAbono(tipoabono);
                        c.setPHsuelo(phsuelo);
                        c.setTextura(textura);
                        c.setTipoFumigacion(tipofumigacion);
                        c.setProductosFumigacion(productosfumigacion);
                        c.setFecharegistro(CultivosSeleccionados.getFecharegistro());
                        c.setTimestamp(CultivosSeleccionados.getTimestamp());
                        databaseReference.child("Cultivos").child(c.getIdcultivo()).setValue(c);
                        Toast.makeText(this, "Actualizado Correctamente", Toast.LENGTH_LONG).show();
                        linearLayoutEditarCultivos.setVisibility(View.GONE);
                        CultivosSeleccionados = null;
                    }
                }else{
                    Toast.makeText(this, "Seleccione un Cultivo", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.menu_eliminar:
                if(CultivosSeleccionados!=null){
                    Cultivos c2 = new Cultivos();
                    c2.setIdcultivo(CultivosSeleccionados.getIdcultivo());
                    databaseReference.child("Cultivos").child(c2.getIdcultivo()).removeValue();
                    linearLayoutEditarCultivos.setVisibility(View.GONE);
                    CultivosSeleccionados = null;
                    Toast.makeText(this, "Eliminado Correctamente", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this, "Seleccione un Cultivo para eliminar", Toast.LENGTH_LONG).show();

                }
                case R.id.home:
                home();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void home() {
        Intent intenthome = new Intent(this, MainActivity.class);
        startActivity(intenthome);
    }

    public void insertarCultivo(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(
                activity_anadir_cultivo.this);
        View mView = getLayoutInflater().inflate(R.layout.ingresarcultivo, null);
        Button btnInsertar = (Button) mView.findViewById(R.id.btnInsertar);
        final EditText mInputNombreFinca = (EditText) mView.findViewById(R.id.inputNombreFinca);
        final EditText mInputNombreCultivo = (EditText) mView.findViewById(R.id.inputNombreCultivo);
        final EditText mInputHectareas = (EditText) mView.findViewById(R.id.inputHectareas);
        final EditText mInputUbicacionFinca = (EditText) mView.findViewById(R.id.inputUbicacionFinca);
        final EditText mInputArbolesH = (EditText) mView.findViewById(R.id.inputArbolesH);
        final EditText mInputTipoCultivo = (EditText) mView.findViewById(R.id.inputTipoCultivo);
        final EditText mInputTipoSuelo = (EditText) mView.findViewById(R.id.inputTipoSuelo);
        final EditText mInputTipoAbono = (EditText) mView.findViewById(R.id.inputTipoAbono);
        final EditText mInputPHsuelo = (EditText) mView.findViewById(R.id.inputPHsuelo);
        final EditText mInputTextura = (EditText) mView.findViewById(R.id.inputTextura);
        final EditText mInputTipoFumigacion = (EditText) mView.findViewById(R.id.inputTipoFumigacion);
        final EditText mInputProductosFumigacion = (EditText) mView.findViewById(R.id.inputProductosFumigacion);

        mBuilder.setView(mView);
        final  AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombrefinca = mInputNombreFinca.getText().toString();
                String nombrecultivo = mInputNombreCultivo.getText().toString();
                String hectareas = mInputHectareas.getText().toString();
                String ubicacionfinca = mInputUbicacionFinca.getText().toString();
                String arbolesh = mInputArbolesH.getText().toString();
                String tipocultivo = mInputTipoCultivo.getText().toString();
                String tiposuelo = mInputTipoSuelo.getText().toString();
                String tipoabono = mInputTipoAbono.getText().toString();
                String phsuelo = mInputPHsuelo.getText().toString();
                String textura = mInputTextura.getText().toString();
                String tipofumigacion = mInputTipoFumigacion.getText().toString();
                String productosfumigacion = mInputProductosFumigacion.getText().toString();
                if(nombrefinca.isEmpty() || nombrefinca.length()<3){
                    showError(mInputNombreFinca, "Nombre invalido (Min. 3 letras)");
                }else if(nombrecultivo.isEmpty() || nombrecultivo.length() <3 ){
                    showError(mInputNombreCultivo, "Nombre invalido (Min. 3 letras)");
                }else if (hectareas.isEmpty() || hectareas.length() < 3) {
                    showError(mInputHectareas, "Nombre invalido. (Min 3 letras)");
                } else if(ubicacionfinca.isEmpty() || ubicacionfinca.length() < 3){
                    showError(mInputUbicacionFinca, "Nombre invalido. (Min 3 letras)");
                } else if(arbolesh.isEmpty() || arbolesh.length() < 3){
                    showError(mInputArbolesH, "Nombre invalido. (Min 3 letras)");
                } else if(tiposuelo.isEmpty() || tiposuelo.length() < 3){
                    showError(mInputTipoSuelo, "Nombre invalido. (Min 3 letras)");
                } else if(tipocultivo.isEmpty() || tipocultivo.length() < 3){
                    showError(mInputTipoCultivo, "Nombre invalido. (Min 3 letras)");
                } else if(tipoabono.isEmpty() || tipoabono.length() < 3){
                    showError(mInputTipoAbono, "Nombre invalido. (Min 3 letras)");
                } else if(phsuelo.isEmpty() || phsuelo.length() < 1){
                    showError(mInputPHsuelo, "Nombre invalido. (Min 1 letras)");
                } else if(textura.isEmpty() || textura.length() < 3){
                    showError(mInputTextura, "Nombre invalido. (Min 3 letras)");
                } else if(tipofumigacion.isEmpty() || tipofumigacion.length() < 3){
                    showError(mInputTipoFumigacion, "Nombre invalido. (Min 3 letras)");
                } else if(productosfumigacion.isEmpty() || productosfumigacion.length() < 3){
                    showError(mInputProductosFumigacion, "Nombre invalido. (Min 3 letras)");
                } else{
                    Cultivos c = new Cultivos();
                    c.setIdcultivo(UUID.randomUUID().toString());
                    c.setNombreFinca(nombrefinca);
                    c.setNombreCultivo(nombrecultivo);
                    c.setHectareas(hectareas);
                    c.setUbicacionFinca(ubicacionfinca);
                    c.setArbolesH(arbolesh);
                    c.setTipoCultivo(tipocultivo);
                    c.setTipoSuelo(tiposuelo);
                    c.setTipoAbono(tipoabono);
                    c.setPHsuelo(phsuelo);
                    c.setTextura(textura);
                    c.setTipoFumigacion(tipofumigacion);
                    c.setProductosFumigacion(productosfumigacion);
                    c.setFecharegistro(getFechaNormal(getFechaMilisegundos()));
                    c.setTimestamp(getFechaMilisegundos() * -1);
                    databaseReference.child("Cultivos").child(c.getIdcultivo()).setValue(c);
                    Toast.makeText(activity_anadir_cultivo.this, "Registrado Correctamente", Toast.LENGTH_LONG).show();

                    dialog.dismiss();
                }
            }
        });

    }

    private void showError(EditText input, String s) {
        input.requestFocus();
        input.setError(s);
    }
    public long getFechaMilisegundos(){
        Calendar calendar =Calendar.getInstance();
        long tiempounix = calendar.getTimeInMillis();

        return tiempounix;
    }
    public String getFechaNormal(long fechamilisegundos){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-5"));
        String fecha = sdf.format(fechamilisegundos);
        return fecha;
    }
    public boolean validarInputs() {
        String nombrefinca = inputNombreFinca.getText().toString();
        String nombrecultivo = inputNombreCultivo.getText().toString();
        String hectareas = inputHectareas.getText().toString();
        String ubicacionfinca = inputUbicacionFinca.getText().toString();
        String arbolesh = inputArbolesH.getText().toString();
        String tipocultivo = inputTipoCultivo.getText().toString();
        String tiposuelo = inputTipoSuelo.getText().toString();
        String tipoabono = inputTipoAbono.getText().toString();
        String phsuelo = inputPHsuelo.getText().toString();
        String textura = inputTextura.getText().toString();
        String tipofumigacion = inputTipoFumigacion.getText().toString();
        String productosfumigacion = inputProductosFumigacion.getText().toString();


        if (nombrefinca.isEmpty() || nombrefinca.length() < 3) {
            showError(inputNombreFinca, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if (nombrecultivo.isEmpty() || nombrecultivo.length() < 3) {
            showError(inputNombreCultivo, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if (hectareas.isEmpty() || hectareas.length() < 3) {
            showError(inputHectareas, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(ubicacionfinca.isEmpty() || ubicacionfinca.length() < 3){
            showError(inputUbicacionFinca, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(arbolesh.isEmpty() || arbolesh.length() < 3){
            showError(inputArbolesH, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(tiposuelo.isEmpty() || tiposuelo.length() < 3){
            showError(inputTipoSuelo, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(tipocultivo.isEmpty() || tipocultivo.length() < 3){
            showError(inputTipoCultivo, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(tipoabono.isEmpty() || tipoabono.length() < 3){
            showError(inputTipoAbono, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(phsuelo.isEmpty() || phsuelo.length() < 1){
            showError(inputPHsuelo, "Nombre invalido. (Min 1 letras)");
            return true;
        } else if(textura.isEmpty() || textura.length() < 3){
            showError(inputTextura, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(tipofumigacion.isEmpty() || tipofumigacion.length() < 3){
            showError(inputTipoFumigacion, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if(productosfumigacion.isEmpty() || productosfumigacion.length() < 3){
            showError(inputProductosFumigacion, "Nombre invalido. (Min 3 letras)");
            return true;
        }
        else{
            return false;
        }
    }
}
