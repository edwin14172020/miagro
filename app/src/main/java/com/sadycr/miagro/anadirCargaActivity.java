package com.sadycr.miagro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sadycr.miagro.Adaptadores.ListViewCargaAdapter;
import com.sadycr.miagro.Models.Carga;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

public class anadirCargaActivity extends AppCompatActivity {

    private ArrayList<Carga> listCarga = new ArrayList<Carga>();
    ArrayAdapter<Carga> arrayAdapterCarga;
    ListViewCargaAdapter listViewCargaAdapter;
    LinearLayout linearLayoutEditar;
    ListView listViewCarga;

    Toolbar toolbar;

    EditText inputFinca, inputCultivo, inputRecolector, inputPesoCarga, inputEncargado;
    Button btnCancelar;

    Carga cargaSeleccionada;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir_carga);

        inputFinca = findViewById(R.id.inputFinca);
        inputCultivo = findViewById(R.id.inputCultivo);
        inputRecolector = findViewById(R.id.inputRecolector);
        inputPesoCarga = findViewById(R.id.inputPesoCarga);
        inputEncargado = findViewById(R.id.inputEncargado);
        btnCancelar = findViewById(R.id.btnCancelar);


        toolbar = findViewById(R.id.toolsbar);
        setSupportActionBar(toolbar);

        listViewCarga = findViewById(R.id.listViewCarga);
        linearLayoutEditar = findViewById(R.id.linearLayoutEditar);

        listViewCarga.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cargaSeleccionada = (Carga) parent.getItemAtPosition(position);
                inputFinca.setText(cargaSeleccionada.getFinca());
                inputCultivo.setText(cargaSeleccionada.getCultivo());
                inputRecolector.setText(cargaSeleccionada.getRecolector());
                inputPesoCarga.setText(cargaSeleccionada.getCarga());
                inputEncargado.setText(cargaSeleccionada.getEncargado());

                linearLayoutEditar.setVisibility(View.VISIBLE);
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutEditar.setVisibility(View.GONE);
                cargaSeleccionada = null;
            }
        });

        inicializarFirebase();
        listarCarga();
    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    private void listarCarga() {
        databaseReference.child("Carga").orderByChild("timestamp").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listCarga.clear();
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {
                    Carga c = objSnaptshot.getValue(Carga.class);
                    listCarga.add(c);
                }
                listViewCargaAdapter = new ListViewCargaAdapter(anadirCargaActivity.this, listCarga);
                listViewCarga.setAdapter(listViewCargaAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_perso, menu);

        MenuItem menuItem = menu.findItem(R.id.buscar);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                listViewCargaAdapter.getFilter().filter(newText);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String finca = inputFinca.getText().toString();
        String cultivo = inputCultivo.getText().toString();
        String recolector = inputRecolector.getText().toString();
        String pesocarga = inputPesoCarga.getText().toString();
        String encargado = inputEncargado.getText().toString();
        switch (item.getItemId()) {

            case R.id.menu_agregar:
                insertar();
                break;
            case R.id.menu_guardar:
                if (cargaSeleccionada != null) {
                    if (validarInputs() == false) {
                        Carga c = new Carga();
                        c.setIdcarga(cargaSeleccionada.getIdcarga());
                        c.setFinca(finca);
                        c.setCultivo(cultivo);
                        c.setRecolector(recolector);
                        c.setCarga(pesocarga);
                        c.setEncargado(encargado);
                        c.setFecharegistro(cargaSeleccionada.getFecharegistro());
                        c.setTimestamp(cargaSeleccionada.getTimestamp());
                        databaseReference.child("Carga").child(c.getIdcarga()).setValue(c);
                        Toast.makeText(this, "Actualizado Correctamente", Toast.LENGTH_LONG).show();
                        linearLayoutEditar.setVisibility(View.GONE);
                        cargaSeleccionada = null;
                    }
                } else {
                    Toast.makeText(this, "Seleccione una Carga", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.menu_eliminar:
                if (cargaSeleccionada != null) {
                    Carga c2 = new Carga();
                    c2.setIdcarga(cargaSeleccionada.getIdcarga());
                    databaseReference.child("Carga").child(c2.getIdcarga()).removeValue();
                    linearLayoutEditar.setVisibility(View.GONE);
                    cargaSeleccionada = null;
                    Toast.makeText(this, "Eliminado Correctamente", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Seleccione una Carga para eliminar", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.home:
                home();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void home() {
        Intent intenthome = new Intent(this, MainActivity.class);
        startActivity(intenthome);
    }

    public void insertar() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(
                anadirCargaActivity.this
        );
        View mView = getLayoutInflater().inflate(R.layout.carga, null);
        Button btnInsertar = (Button) mView.findViewById(R.id.btnInsertar);
        final EditText mInputFinca = (EditText) mView.findViewById(R.id.inputFinca);
        final EditText mInputCultivo = (EditText) mView.findViewById(R.id.inputCultivo);
        final EditText mInputRecolector = (EditText) mView.findViewById(R.id.inputRecolector);
        final EditText mInputPesoCarga = (EditText) mView.findViewById(R.id.inputPesoCarga);
        final EditText mInputEncargado = (EditText) mView.findViewById(R.id.inputEncargado);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String finca = mInputFinca.getText().toString();
                String cultivo = mInputCultivo.getText().toString();
                String recolector = mInputRecolector.getText().toString();
                String pesocarga = mInputPesoCarga.getText().toString();
                String encargado = mInputEncargado.getText().toString();
                if (finca.isEmpty() || finca.length() < 3) {
                    showError(mInputFinca, "Nombre invalido (Min. 3 letras)");
                } else if (cultivo.isEmpty() || cultivo.length() < 3) {
                    showError(mInputCultivo, "Nombre invalido (Min. 3 letras)");
                } else if (recolector.isEmpty() || recolector.length() < 3) {
                    showError(mInputRecolector, "Nombre invalido (Min. 9 letras)");
                } else if (pesocarga.isEmpty() || pesocarga.length() < 2) {
                    showError(mInputPesoCarga, "Numero invalido (Min. 2 Números)");
                } else if (encargado.isEmpty() || encargado.length() < 3) {
                    showError(mInputEncargado, "Nombre invalido (Min. 9 letras)");
                } else {
                    Carga c = new Carga();
                    c.setIdcarga(UUID.randomUUID().toString());
                    c.setFinca(finca);
                    c.setCultivo(cultivo);
                    c.setRecolector(recolector);
                    c.setCarga(pesocarga);
                    c.setEncargado(encargado);
                    c.setFecharegistro(getFechaNormal(getFechaMilisegundos()));
                    c.setTimestamp(getFechaMilisegundos() * -1);
                    databaseReference.child("Carga").child(c.getIdcarga()).setValue(c);
                    Toast.makeText(anadirCargaActivity.this, "Registrado Correctamente", Toast.LENGTH_LONG).show();

                    dialog.dismiss();
                }
            }
        });


    }

    public void showError(EditText input, String s) {
        input.requestFocus();
        input.setError(s);
    }

    public long getFechaMilisegundos() {
        Calendar calendar = Calendar.getInstance();
        long tiempounix = calendar.getTimeInMillis();

        return tiempounix;
    }

    public String getFechaNormal(long fechamilisegundos) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-5"));
        String fecha = sdf.format(fechamilisegundos);
        return fecha;
    }

    public boolean validarInputs() {
        String finca = inputFinca.getText().toString();
        String cultivo = inputCultivo.getText().toString();
        String recolector = inputRecolector.getText().toString();
        String pesocarga = inputPesoCarga.getText().toString();
        String encargado = inputEncargado.getText().toString();
        if (finca.isEmpty() || finca.length() < 3) {
            showError(inputFinca, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if (cultivo.isEmpty() || cultivo.length() < 3) {
            showError(inputCultivo, "Nombre invalido (Min 3 letras)");
            return true;
        } else if (recolector.isEmpty() || recolector.length() < 3) {
            showError(inputRecolector, "Nombre invalido (Min 3 letras)");
            return true;
        } else if (pesocarga.isEmpty() || pesocarga.length() < 2) {
            showError(inputPesoCarga, "Nombre invalido (Min 2 Número)");
            return true;
        } else if (encargado.isEmpty() || encargado.length() < 3) {
            showError(inputEncargado, "Nombre invalido (Min 3 letras)");
            return true;
        } else {
            return false;
        }
    }
}

